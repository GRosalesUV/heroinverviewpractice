import React, { useState } from 'react';
import { createSubscribe } from '../helpers/subscribeManager';

const Form = ({ setSubscribeStatus }) => {
  const [formValues, setFormValues] = useState({
    firstName: {
      value: '',
      required: true,
      valid: '',
      error: ''
    },
    lastName: {
      value: '',
      required: true,
      valid: '',
      error: ''
    },
    email: {
      value: '',
      required: true,
      pattern: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
      valid: '',
      error: ''
    },
    fieldName: {
      value: '',
      required: true,
      valid: '',
      error: ''
    },
    org: {
      value: '',
      required: false,
      valid: '',
      error: ''
    },
    euResident: {
      value: '',
      required: true,
      valid: '',
      error: ''
    }
  });
  const handleOnChange = ({ target }) => {
    const currentValue = formValues[target.name];

    if (target.type === 'checkbox') {
      currentValue.value = (
        target.checked
          ? addCheckboxValue(currentValue.value, target.value)
          : removeCheckboxValue(currentValue.value, target.value)
      );
    } else {
      currentValue.value = target.value;
    }

    const [valid, error] = validateField(currentValue);

    currentValue.valid = valid;
    currentValue.error = error;

    setFormValues({ ...formValues, ...{ [target.name]: currentValue } });
    return;
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const newFormValues = formValues;
    const valuesFormatted = {};

    // Validate fields
    Object.entries(newFormValues).forEach(([key, val]) => {
      const [valid, errorMessage] = validateField(val);

      val.valid = valid;
      val.error = errorMessage;

      newFormValues[key] = { ...newFormValues[key], ...val };
      return;
    });

    // Update formValues state
    setFormValues((prevState) => ({ ...prevState, ...newFormValues }));

    // All are valid?
    if (!Object.entries(newFormValues).every(([key, val]) => (val.valid))) {
      console.log('Some values are\'nt valid');
      return;
    }

    Object.entries(newFormValues).forEach(([key, val]) => {
      valuesFormatted[key] = val.value;
    });

    const resp = await createSubscribe(valuesFormatted);

    setSubscribeStatus((prevState) => ({ ...prevState, ...resp }));
    return;
  };
  const removeCheckboxValue = (values, value) => {
    const valueArr = values.split(',');
    const newValueArr = valueArr.filter(val => val !== value);

    return newValueArr.join(',');
  };
  const addCheckboxValue = (values, value) => {
    const valueArr = values.split(',').filter((item) => item);

    valueArr.push(value);

    return valueArr.join(',');
  };
  const validateField = (field) => {
    if (!field.required) {
      return [true, ''];
    }

    if (field.pattern) {
      if (!field.value.match(field.pattern)) {
        return [false, 'Insert a valid email.'];
      }
    }

    return (field.value.length === 0 ? [false, 'This field is required'] : [true, '']);
  };
  const handleReset = () => {
    const values = formValues;

    Object.entries(values).forEach(([key, val]) => {
      const replacedValues = { value: '', valid: '', error: '' };

      values[key] = { ...val, ...replacedValues };
    });

    setFormValues((prevState) => ({ ...prevState, ...values }));
    return;
  };

  return (
    <>
      <h1>Sign up for email updates</h1>
      <p>* Indicates Required Field</p>
      <form onSubmit={handleSubmit} onReset={handleReset}>
        <div className="form-group">
          <div className="input-group">
            <span className={formValues.firstName.valid === false ? 'invalid' : ''}>{formValues.firstName.error}</span>
            <label htmlFor="form-firstName">first name*</label>
            <input name="firstName" type="text" id="form-firstName" className={formValues.firstName.valid === false ? 'invalid' : ''} onChange={handleOnChange} autoFocus />
          </div>
          <div className="input-group">
            <span className={formValues.lastName.valid === false ? 'invalid' : ''}>{formValues.lastName.error}</span>
            <label htmlFor="form-lastName">Last name*</label>
            <input id="form-lastName" name="lastName" type="text" className={formValues.lastName.valid === false ? 'invalid' : ''} onChange={handleOnChange} />
          </div>
          <div className="input-group">
            <span className={formValues.email.valid === false ? 'invalid' : ''}>{formValues.email.error}</span>
            <label htmlFor="form-email">email address*</label>
            <input id="form-email" name="email" type="email" className={formValues.email.valid === false ? 'invalid' : ''} onChange={handleOnChange} />
          </div>
          <div className="input-group">
            <span className={formValues.org.valid === false ? 'invalid' : ''}>{formValues.org.error}</span>
            <label htmlFor="form-org">organization</label>
            <input id="form-org" name="org" type="text" className={formValues.org.valid === false ? 'invalid' : ''} onChange={handleOnChange} />
          </div>
          <div className="input-group">
            <span className={formValues.euResident.valid === false ? 'invalid' : ''}>{formValues.euResident.error}</span>
            <label htmlFor="form-euResident">eu resident*</label>
            <select id="form-euResident" name="euResident" className={formValues.euResident.valid === false ? 'invalid' : ''} onChange={handleOnChange} >
              <option value="">- Select one -</option>
              <option value="yes">yes</option>
              <option value="no">no</option>
            </select>
          </div>
        </div>
        <div className="form-group">
          <span className={formValues.fieldName.valid === false ? 'invalid' : ''}>{formValues.fieldName.error}</span>
          <div className="input-group">
            <input type="checkbox" name="fieldName" id="form-fieldName-advances" onChange={handleOnChange} value="advances" />
            <label htmlFor="form-fieldName-advances">advances</label>
          </div>
          <div className="input-group">
            <input type="checkbox" name="fieldName" id="form-fieldName-alerts" onChange={handleOnChange} value="alerts" />
            <label htmlFor="form-fieldName-alerts">alerts</label>
          </div>
          <div className="input-group">
            <input type="checkbox" name="fieldName" id="form-fieldName-other" onChange={handleOnChange} value="other" />
            <label htmlFor="form-fieldName-other">other communications</label>
          </div>
        </div>
        <div className="actions">
          <button type="submit">Submit</button>
          <button type="reset">Reset</button>
        </div>
      </form >
    </>
  );
};

export default Form;
