'use strict';

import React from 'react';

const FlashMessages = ({ subscribeStatus, setSubscribeStatus }) => {
  const resetSubscribeStatus = () => {
    setSubscribeStatus((prevState) => ({ ...prevState, ...{ status: '', message: '' } }));
    return;
  };

  return (
    <>
      <div className={subscribeStatus.status === "success" ? "success" : "alert"}>
        <p>{subscribeStatus.message}</p>
      </div>
      <button className="center" onClick={resetSubscribeStatus}>Show form</button>
    </>
  );
};

export default FlashMessages;
