'use strict';

const API_URL = `${window.origin}/api/subscribe`;

const createSubscribe = async (data) => {
  const formData = populateFormData(data);
  const resp = await fetch(API_URL, {
    method: 'POST',
    mode: 'same-origin',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: formData
  });

  if (!resp.ok) {
    return { status: "error", message: 'Server unreached, try again later.' };
  }

  if (resp.status !== 200) {
    return { status: 'error', message: 'Server error, we\'re working to solve it. :)' }
  }

  return await resp.json();
};

const populateFormData = (objectFormData) => {
  const formData = new FormData();

  Object.entries(objectFormData).forEach(([key, val]) => formData.append(key, val));

  return formData;
};

export { createSubscribe };
