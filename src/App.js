import React, { useState } from 'react';
import Form from './components/Form';
import FlashMessages from './components/FlashMessages';

const App = () => {
  const [subscribeStatus, setSubscribeStatus] = useState({ status: '', message: '' });

  return (
    <div className="container">
      {
        subscribeStatus.status
          ? <FlashMessages subscribeStatus={subscribeStatus} setSubscribeStatus={setSubscribeStatus} />
          : <Form setSubscribeStatus={setSubscribeStatus} />
      }
    </div>
  );
};

export default App;