const express = require('express');
const debug = require('debug')('Server');
const path = require('path');

const PORT = 3000;

const app = express();

app.use('/assets', express.static(path.join(__dirname, 'public', 'assets')));
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});
app.post('/api/subscribe', (req, res) => {
  // Dummy response
  if (Math.round(Math.random())) {
    debug('Subscribed');
    return res.status(200).json({ status: 'success', message: 'Thank you. You are now subscribed.' });
  }

  debug('Error! ;)');
  return res.status(404).json({ status: 'error', message: 'Thank you. You are now subscribed.' });
})

app.listen(PORT, () => debug('Server started'));
